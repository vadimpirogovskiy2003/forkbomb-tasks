BITS 64
_start:
    xor rax, rax
    cdq

    mov al, 41
    xor rdi, rdi
    inc rdi
    inc rdi
    xor rsi, rsi
    inc rsi
    syscall

    mov rcx, rax ;rsi = fd

    mov byte[rsp], 0x7f
    xor rbx, rbx
    mov byte[rsp+1], bl
    mov byte[rsp+2], bl
    mov byte[rsp+3], 0x1
    push word 0x697a
    push word 2
    mov rsi, rsp
    mov rdi, rcx
    push 0x10
    pop rdx
    xor rax, rax
    mov al, 42
    syscall

    mov rdx, 0x333
    sub rsp, rdx
    mov rsi, rsp
    xor rax, rax
    syscall

    xor rdi, rdi
    inc rdi
    xor rax, rax
    inc rax
    syscall
    
