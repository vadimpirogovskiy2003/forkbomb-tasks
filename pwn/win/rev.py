import sys
from qiling import Qiling
from qiling.extensions import pipe

def dump(ql, *args, **kw):
    ql.save(reg=True, cpu_context=True, snapshot="./snapshot.bin")
    ql.emu_stop()

if __name__ == "__main__":
    ql = Qiling(["./win.elf"], "~/rootfs")
    ql.os.stdin = pipe.SimpleInStream(sys.stdin.fileno())
    f = open("./shellcode", 'rb')
    pl = f.read()
    ql.os.stdin.write(b'\x90')
    BASE = int(ql.profile.get("OS64", "load_address"), 16)
    print("BASE:", hex(BASE))
    ql.hook_address(dump, BASE + 0x12ad)
    ql.run()
